FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/swagger-spring-1.0.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
