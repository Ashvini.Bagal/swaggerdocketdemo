package io.swagger.repository;


import io.swagger.document.PersonDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PersonRepository extends ElasticsearchRepository<PersonDoc, String> {


}
