package io.swagger.repository;

import io.swagger.document.EducationDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EducationRepository extends ElasticsearchRepository<EducationDoc,String> {
}
