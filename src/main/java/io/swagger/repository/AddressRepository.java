package io.swagger.repository;

import io.swagger.document.AddressDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface AddressRepository extends ElasticsearchRepository<AddressDoc,String> {
}
