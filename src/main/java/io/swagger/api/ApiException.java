package io.swagger.api;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-07-28T02:56:17.552Z[GMT]")
public class ApiException extends Exception {
    private int code;

    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
