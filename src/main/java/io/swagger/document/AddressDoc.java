package io.swagger.document;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "address")
@Data
@Builder

public class AddressDoc {

    @Id
    @Field(type = FieldType.Keyword)
    private String id;

    @Field(type = FieldType.Text)
    private String keclockId;


    @Field(type = FieldType.Text)
    private String addressDetails ;


    @Field(type = FieldType.Text)
    private String addressDetails1;

    @Field(type = FieldType.Text)
    private String city ;


    @Field(type = FieldType.Text)
    private String district;


    @Field(type = FieldType.Text)
    private String state;

    @Field(type = FieldType.Text)
    private String pinCode;

    @Field(type = FieldType.Text)
    private String country;



}
