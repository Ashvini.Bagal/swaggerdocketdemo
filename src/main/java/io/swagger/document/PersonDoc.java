package io.swagger.document;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "doctor")
@Data
@Builder
public class PersonDoc {
    @Id
    @Field(type = FieldType.Keyword)
    private String id;

    @Field(type = FieldType.Text)
    private String firstName;


    @Field(type = FieldType.Text)
    private String middleName;


    @Field(type = FieldType.Text)
    private String lastName;

    @Field(type = FieldType.Text)
    private String email;


    @Field(type = FieldType.Text)
    private String userName;


    @Field(type = FieldType.Text)
    private String gender;

    @Field(type = FieldType.Text)
    private String location;

    @Field(type = FieldType.Text)
    private String mobile;

    @Field(type = FieldType.Text)
    private String personType;

    @Field(type = FieldType.Text)
    private String profilePhoto;

    @Field(type = FieldType.Text)
    private String dateOfBirth;
}

