package io.swagger.document;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Document(indexName = "education")
@Data
@Builder
public class EducationDoc {
    @Id

    @Field(type = FieldType.Keyword)
    private String id;

    @Field(type = FieldType.Text)
    private String keclockId;


    @Field(type = FieldType.Text)
    private String educationDetails;


    @Field(type = FieldType.Text)
    private String educationDetails1;

    @Field(type = FieldType.Text)
    private String highestDegree;


    @Field(type = FieldType.Text)
    private String workExperience;


    @Field(type = FieldType.Text)
    private String specialization;


}
