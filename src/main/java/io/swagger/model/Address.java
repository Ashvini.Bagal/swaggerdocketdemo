package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Address
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-07-28T02:56:17.552Z[GMT]")


public class Address   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("keclockId")
  private String keclockId = null;

  @JsonProperty("addressDetails")
  private String addressDetails = null;

  @JsonProperty("addressDetails1")
  private String addressDetails1 = null;

  @JsonProperty("city")
  private String city = null;

  @JsonProperty("district")
  private String district = null;

  @JsonProperty("state")
  private String state = null;

  @JsonProperty("pincode")
  private String pincode = null;

  @JsonProperty("country")
  private String country = null;

  public Address id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Address keclockId(String keclockId) {
    this.keclockId = keclockId;
    return this;
  }

  /**
   * Get keclockId
   * @return keclockId
   **/
  @Schema(description = "")
  
    public String getKeclockId() {
    return keclockId;
  }

  public void setKeclockId(String keclockId) {
    this.keclockId = keclockId;
  }

  public Address addressDetails(String addressDetails) {
    this.addressDetails = addressDetails;
    return this;
  }

  /**
   * Get addressDetails
   * @return addressDetails
   **/
  @Schema(description = "")
  
    public String getAddressDetails() {
    return addressDetails;
  }

  public void setAddressDetails(String addressDetails) {
    this.addressDetails = addressDetails;
  }

  public Address addressDetails1(String addressDetails1) {
    this.addressDetails1 = addressDetails1;
    return this;
  }

  /**
   * Get addressDetails1
   * @return addressDetails1
   **/
  @Schema(description = "")
  
    public String getAddressDetails1() {
    return addressDetails1;
  }

  public void setAddressDetails1(String addressDetails1) {
    this.addressDetails1 = addressDetails1;
  }

  public Address city(String city) {
    this.city = city;
    return this;
  }

  /**
   * Get city
   * @return city
   **/
  @Schema(description = "")
  
    public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public Address district(String district) {
    this.district = district;
    return this;
  }

  /**
   * Get district
   * @return district
   **/
  @Schema(description = "")
  
    public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

  public Address state(String state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
   **/
  @Schema(description = "")
  
    public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Address pincode(String pincode) {
    this.pincode = pincode;
    return this;
  }

  /**
   * Get pincode
   * @return pincode
   **/
  @Schema(description = "")
  
    public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public Address country(String country) {
    this.country = country;
    return this;
  }

  /**
   * Get country
   * @return country
   **/
  @Schema(description = "")
  
    public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(this.id, address.id) &&
        Objects.equals(this.keclockId, address.keclockId) &&
        Objects.equals(this.addressDetails, address.addressDetails) &&
        Objects.equals(this.addressDetails1, address.addressDetails1) &&
        Objects.equals(this.city, address.city) &&
        Objects.equals(this.district, address.district) &&
        Objects.equals(this.state, address.state) &&
        Objects.equals(this.pincode, address.pincode) &&
        Objects.equals(this.country, address.country);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, keclockId, addressDetails, addressDetails1, city, district, state, pincode, country);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    keclockId: ").append(toIndentedString(keclockId)).append("\n");
    sb.append("    addressDetails: ").append(toIndentedString(addressDetails)).append("\n");
    sb.append("    addressDetails1: ").append(toIndentedString(addressDetails1)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    district: ").append(toIndentedString(district)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("    pincode: ").append(toIndentedString(pincode)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
