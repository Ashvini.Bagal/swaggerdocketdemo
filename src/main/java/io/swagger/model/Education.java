package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Education
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-07-28T02:56:17.552Z[GMT]")


public class Education   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("keclockId")
  private String keclockId = null;

  @JsonProperty("educationDetails")
  private String educationDetails = null;

  @JsonProperty("educationDetails1")
  private String educationDetails1 = null;

  @JsonProperty("highestDegree")
  private String highestDegree = null;

  @JsonProperty("workExperience")
  private String workExperience = null;

  @JsonProperty("specialization")
  private String specialization = null;

  public Education id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
   **/
  @Schema(description = "")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Education keclockId(String keclockId) {
    this.keclockId = keclockId;
    return this;
  }

  /**
   * Get keclockId
   * @return keclockId
   **/
  @Schema(description = "")
  
    public String getKeclockId() {
    return keclockId;
  }

  public void setKeclockId(String keclockId) {
    this.keclockId = keclockId;
  }

  public Education educationDetails(String educationDetails) {
    this.educationDetails = educationDetails;
    return this;
  }

  /**
   * Get educationDetails
   * @return educationDetails
   **/
  @Schema(description = "")
  
    public String getEducationDetails() {
    return educationDetails;
  }

  public void setEducationDetails(String educationDetails) {
    this.educationDetails = educationDetails;
  }

  public Education educationDetails1(String educationDetails1) {
    this.educationDetails1 = educationDetails1;
    return this;
  }

  /**
   * Get educationDetails1
   * @return educationDetails1
   **/
  @Schema(description = "")
  
    public String getEducationDetails1() {
    return educationDetails1;
  }

  public void setEducationDetails1(String educationDetails1) {
    this.educationDetails1 = educationDetails1;
  }

  public Education highestDegree(String highestDegree) {
    this.highestDegree = highestDegree;
    return this;
  }

  /**
   * Get highestDegree
   * @return highestDegree
   **/
  @Schema(description = "")
  
    public String getHighestDegree() {
    return highestDegree;
  }

  public void setHighestDegree(String highestDegree) {
    this.highestDegree = highestDegree;
  }

  public Education workExperience(String workExperience) {
    this.workExperience = workExperience;
    return this;
  }

  /**
   * Get workExperience
   * @return workExperience
   **/
  @Schema(description = "")
  
    public String getWorkExperience() {
    return workExperience;
  }

  public void setWorkExperience(String workExperience) {
    this.workExperience = workExperience;
  }

  public Education specialization(String specialization) {
    this.specialization = specialization;
    return this;
  }

  /**
   * Get specialization
   * @return specialization
   **/
  @Schema(description = "")
  
    public String getSpecialization() {
    return specialization;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Education education = (Education) o;
    return Objects.equals(this.id, education.id) &&
        Objects.equals(this.keclockId, education.keclockId) &&
        Objects.equals(this.educationDetails, education.educationDetails) &&
        Objects.equals(this.educationDetails1, education.educationDetails1) &&
        Objects.equals(this.highestDegree, education.highestDegree) &&
        Objects.equals(this.workExperience, education.workExperience) &&
        Objects.equals(this.specialization, education.specialization);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, keclockId, educationDetails, educationDetails1, highestDegree, workExperience, specialization);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Education {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    keclockId: ").append(toIndentedString(keclockId)).append("\n");
    sb.append("    educationDetails: ").append(toIndentedString(educationDetails)).append("\n");
    sb.append("    educationDetails1: ").append(toIndentedString(educationDetails1)).append("\n");
    sb.append("    highestDegree: ").append(toIndentedString(highestDegree)).append("\n");
    sb.append("    workExperience: ").append(toIndentedString(workExperience)).append("\n");
    sb.append("    specialization: ").append(toIndentedString(specialization)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
