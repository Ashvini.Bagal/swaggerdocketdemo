package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonUpdate {
    @JsonProperty("id")
    private String id = null;

    @JsonProperty("firstName")
    private String firstName = null;

    @JsonProperty("middleName")
    private String middleName = null;

    @JsonProperty("lastName")
    private String lastName = null;

    @JsonProperty("email")
    private String email = null;

    @JsonProperty("userName")
    private String userName = null;

    @JsonProperty("gender")
    private String gender = null;

    @JsonProperty("location")
    private String location = null;

    @JsonProperty("mobile")
    private String mobile = null;

}
