package io.swagger.service;

import io.swagger.api.ApiException;
import io.swagger.document.PersonDoc;
import io.swagger.model.Person;
import io.swagger.model.PersonUpdate;
import io.swagger.repository.PersonRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    PersonRepository personRepository;

    public Person getPersonbyId(String id) throws ApiException {
        Person person = new Person();
        Optional<PersonDoc> personDoc = personRepository.findById(id);
        if(personDoc.isPresent()) {
            BeanUtils.copyProperties(personDoc.get(), person);
        } else {
            throw new ApiException(400, "Person not found");
        }
        return person;
    }

    public Iterable<PersonDoc> getAllPersons() {
        return personRepository.findAll();
    }

    public PersonDoc createPerson(Person person) throws ApiException {
        PersonDoc personDoc = PersonDoc.builder()
                .id(person.getId())
                .email(person.getEmail())
                .dateOfBirth(person.getDateOfBirth())
                .firstName(person.getFirstName())
                .lastName(person.getLastName())
                .middleName(person.getMiddleName())
                .gender(person.getGender())
                .location(person.getLocation())
                .personType(person.getPersonType())
                .mobile(person.getMobile())
                .profilePhoto(person.getProfilePhoto())
                .userName(person.getUserName())

                .build();

        return personRepository.save(personDoc);

    }
    public String deletePerson(String id) throws ApiException {
        Optional<PersonDoc> personDoc = personRepository.findById(id);
        if (personDoc.isPresent())
            personRepository.deleteById(id);
        else
            throw new ApiException(400, "Person not found");
        return "deleted successfully";
    }

    public PersonDoc updatePerson(PersonUpdate person) throws ApiException {

        Optional<PersonDoc> personDoc = personRepository.findById(person.getId());

        if (personDoc.isPresent()) {
            PersonDoc personData = personDoc.get();
            personData.setFirstName(person.getFirstName());
            personData.setMiddleName(person.getMiddleName());
            personData.setLastName(person.getLastName());
            personData.setEmail(person.getEmail());
            personData.setMobile(person.getMobile());
            personData.setUserName(person.getUserName());
            personData.setGender(person.getGender());
            personData.setLocation(person.getLocation());

            return personRepository.save(personData);
        }else
            throw new ApiException(400, "Person not found");

    }
}

