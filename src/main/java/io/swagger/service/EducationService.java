package io.swagger.service;

import io.swagger.api.ApiException;
import io.swagger.document.AddressDoc;
import io.swagger.document.EducationDoc;
import io.swagger.model.Address;
import io.swagger.model.Education;
import io.swagger.repository.EducationRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
@Service
public class EducationService {
    @Autowired
    EducationRepository educationRepository;

    public Education getEducationById(String id) throws ApiException {
        Education education = new Education();
        Optional<EducationDoc> educationDoc = educationRepository.findById(id);
        if (educationDoc.isPresent()) {
            BeanUtils.copyProperties(educationDoc.get(), education);
        } else {
            throw new ApiException(400, "education id not found");
        }
        return education;
    }


    public Iterable<EducationDoc> getAllEducation() {
        return educationRepository.findAll();
    }

    public EducationDoc createEducation(Education req) throws ApiException {
        EducationDoc educationDoc=EducationDoc.builder()
                .id(req.getId())
                .keclockId(req.getKeclockId())
                .educationDetails(req.getEducationDetails())
                .educationDetails1(req.getEducationDetails1())
                .highestDegree(req.getHighestDegree())
                .specialization(req.getSpecialization())
                .workExperience(req.getWorkExperience())
                .build();
        return educationRepository.save(educationDoc);
    }



    public String deleteEducation(String id) throws ApiException {
        Optional<EducationDoc> educationDoc = educationRepository.findById(id);
        if (educationDoc.isPresent())
            educationRepository.deleteById(id);
        else
            throw new ApiException(400, "Education Id not found");
        return "deleted successfully";
    }


    public Education updateEducation(String id, Education req) throws ApiException {
            Optional<EducationDoc> educationDoc=educationRepository.findById(id);
                BeanUtils.copyProperties(req,educationDoc.get());
                educationRepository.save(educationDoc.get());
            return req;

    }
}
