package io.swagger.service;

import io.swagger.api.ApiException;
import io.swagger.document.AddressDoc;
import io.swagger.model.Address;
import io.swagger.model.Person;
import io.swagger.repository.AddressRepository;
import org.elasticsearch.ResourceNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;
@Service
public class AddressService {
    @Autowired
    AddressRepository addressRepository;

    public AddressDoc createAddress(Address req) throws ApiException {

        AddressDoc address=AddressDoc.builder()
                .id(req.getId())
                .keclockId(req.getKeclockId())
                .addressDetails(req.getAddressDetails())
                .addressDetails1(req.getAddressDetails1())
                .city(req.getCity())
                .country(req.getCountry())
                .state(req.getState())
                .district(req.getDistrict())
                .pinCode(req.getPincode())
                .build();
        return addressRepository.save(address);
    }

    public Iterable<AddressDoc> getAllAddress() {
        return addressRepository.findAll();
    }


    public Address getAddressById(String id) throws ApiException {
        Address address = new Address();
        Optional<AddressDoc> addressDoc = addressRepository.findById(id);
        if (addressDoc.isPresent()) {
            BeanUtils.copyProperties(addressDoc.get(), address);
        } else {
            throw new ApiException(400, "Address not found");
        }
        return address;
    }



    public String deleteAddress(String id) throws ApiException {
        Optional<AddressDoc> addressDoc = addressRepository.findById(id);
        if (addressDoc.isPresent())
            addressRepository.deleteById(id);
        else
            throw new ApiException(400, "Address not found");
        return "Address deleted successfully";
    }


    public Address updateAddress(String id,Address req) throws ApiException {
        Optional<AddressDoc> addressDoc=addressRepository.findById(id);
        if(addressDoc.isPresent())
        {
            BeanUtils.copyProperties(req,addressDoc.get());
            addressRepository.save(addressDoc.get());
        } else {
            throw new ApiException(400, "Address id not found");
        }
       return req;

    }
}

